<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(Request $request){
        return view('register');
    }

    public function welcome(Request $request){
        // dd($request["fname"]);
        $firstName = $request["fname"];
        $lastName = $request["lname"];

        return view('welcome', ["fname" => $firstName, "lname" => $lastName]);
    }
}
