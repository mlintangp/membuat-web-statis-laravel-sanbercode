<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
  </head>
  <body>
    <h1>Buat Account Baru!</h1>

    <h3>Sign Up Form</h3>

    <form action="/welcome" method="post">
      @csrf
      <label for="fname">First name:</label>
      <br /><br />
      <input type="text" name="fname"/>
      <br /><br />

      <label for="lname">Last name:</label>
      <br /><br />
      <input type="text" name="lname"/>
      <br /><br />

      <label for="gender">Gender:</label>
      <br /><br />
      <input type="radio" name="gender" />Male <br />
      <input type="radio" name="gender" />Female <br />
      <input type="radio" name="gender" />Other <br /><br />

      <label for="nationality">Nationality:</label>
      <br /><br />
      <select name="nationality">
        <option>Indonesian</option>
        <option>Singaporean</option>
        <option>Malaysian</option>
        <option>Australian</option>
      </select>
      <br /><br />

      <label for="lang">Language Spoken:</label>
      <br /><br />
      <input type="checkbox" name="lang" />Bahasa Indonesia
      <br />
      <input type="checkbox" name="lang" />English
      <br />
      <input type="checkbox" name="lang" />Other <br /><br />

      <label for="bio">Bio:</label>
      <br /><br />
      <textarea name="bio" cols="30" rows="10"></textarea><br />

      <input type="submit" value="Sign Up" />
    </form>
  </body>
</html>
